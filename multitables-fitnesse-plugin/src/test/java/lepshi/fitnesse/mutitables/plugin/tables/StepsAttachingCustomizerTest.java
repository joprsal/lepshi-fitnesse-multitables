package lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.tables.SlimAssertion;
import lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StepsAttachingCustomizerTest {

    private StepsAttachingCustomizer customizer;

    @Mock
    private EachTestSteps       eachTestSteps;
    @Mock
    private SlimAssertion       in1;
    @Mock
    private SlimAssertion       in2;
    private List<SlimAssertion> inAssertions;

    private List<ActionCombination> beforeCombinations = new ArrayList<>();
    private List<ActionCombination> afterCombinations  = new ArrayList<>();


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);

        inAssertions = asList(in1, in2);
        when(eachTestSteps.getBeforeSteps()).thenReturn(beforeCombinations);
        when(eachTestSteps.getAfterSteps()).thenReturn(afterCombinations);

        customizer = new StepsAttachingCustomizer(eachTestSteps, null);
    }


    @Test
    void should_return_original_assertions_when_no_before_or_after() {
        assertThat(customizer.customize(inAssertions), contains(in1, in2));
    }

    @Test
    void should_prepend_BeforeEach_assertions_when_available() {
        ActionCombination before1 = givenBeforeInContext();
        ActionCombination before2 = givenBeforeInContext();

        List<SlimAssertion> expectedList = new ArrayList<>();
        expectedList.addAll(before1.getAssertions());
        expectedList.addAll(before2.getAssertions());
        expectedList.addAll(inAssertions);

        assertThat(customizer.customize(inAssertions), is(expectedList));
    }

    @Test
    void should_append_AfterEach_assertions_when_available() {
        ActionCombination after1 = givenAfterInContext();
        ActionCombination after2 = givenAfterInContext();

        List<SlimAssertion> expectedList = new ArrayList<>();
        expectedList.addAll(inAssertions);
        expectedList.addAll(after1.getAssertions());
        expectedList.addAll(after2.getAssertions());

        assertThat(customizer.customize(inAssertions), is(expectedList));
    }

    @Test
    void should_append_BeforeEach_and_AfterEach_assertions_when_available() {
        ActionCombination before = givenBeforeInContext();
        ActionCombination after = givenAfterInContext();

        List<SlimAssertion> expectedList = new ArrayList<>();
        expectedList.addAll(before.getAssertions());
        expectedList.addAll(inAssertions);
        expectedList.addAll(after.getAssertions());

        assertThat(customizer.customize(inAssertions), is(expectedList));
    }

    @Test
    void should_not_delegate_to_underlying_expectations_when_result_is_evaluated() {
        ActionCombination before = givenBeforeInContext();
        ActionCombination after = givenAfterInContext();

        before.getAssertions();
        after.getAssertions();
    }


    private ActionCombination givenBeforeInContext() {
        return givenContextWithActionCombination(beforeCombinations, CombinationType.BEFORE_EACH_TEST_STEP);
    }

    private ActionCombination givenAfterInContext() {
        return givenContextWithActionCombination(afterCombinations, CombinationType.AFTER_EACH_TEST_STEP);
    }

    private ActionCombination givenContextWithActionCombination(List<ActionCombination> combinations, CombinationType type) {
        final int idx = combinations.size();
        ActionCombination combination = mock(ActionCombination.class);
        when(combination.getCombinationType()).thenReturn(type);
        when(combination.getAssertions()).thenReturn(asList(mock(SlimAssertion.class), mock(SlimAssertion.class)));
        when(combination.getCombinationTitle()).thenReturn(type.getDisplayName() + " " + idx);
        when(combination.getCombinationIdSupplier()).thenReturn(() -> type.getDisplayName() + "_id" + idx);
        combinations.add(combination);
        return combination;
    }
}