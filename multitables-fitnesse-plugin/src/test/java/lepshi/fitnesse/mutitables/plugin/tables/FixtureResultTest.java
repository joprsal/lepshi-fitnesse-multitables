package lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.TestResult;
import org.junit.Test;

import static fitnesse.testsystems.ExecutionResult.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;


public class FixtureResultTest {

    @Test
    public void should_parse_code_without_message() {
        FixtureResult fixtureResult = FixtureResult.parseFrom("pass");

        assertThat(fixtureResult.code, is("pass"));
        assertThat(fixtureResult.message, is(nullValue()));
    }

    @Test
    public void should_parse_code_with_message() {
        FixtureResult fixtureResult = FixtureResult.parseFrom("fail:error msg");

        assertThat(fixtureResult.code, is("fail"));
        assertThat(fixtureResult.message, is("error msg"));
    }

    @Test
    public void should_map_to_corresponding_ExecutionResult() {
        assertThat(FixtureResult.parseFrom("pass:msg").asExecutionResult(), is(PASS));
        assertThat(FixtureResult.parseFrom("pass").asExecutionResult(), is(PASS));
        assertThat(FixtureResult.parseFrom("ignore:msg").asExecutionResult(), is(IGNORE));
        assertThat(FixtureResult.parseFrom("fail:msg").asExecutionResult(), is(FAIL));
        assertThat(FixtureResult.parseFrom("plain:msg").asExecutionResult(), is(nullValue()));
        assertThat(FixtureResult.parseFrom(":msg").asExecutionResult(), is(nullValue()));
        assertThat(FixtureResult.parseFrom("").asExecutionResult(), is(nullValue()));
    }

    @Test
    public void should_map_to_corresponding_TestResult() {
        final TestResult failTestResult = FixtureResult.parseFrom("fail:msg").asTestResult();
        assertThat(failTestResult.getExecutionResult(), is(FAIL));
        assertThat(failTestResult.hasMessage(), is(true));
        assertThat(failTestResult.getMessage(), is("msg"));

        final TestResult noMsgTestResult = FixtureResult.parseFrom("pass").asTestResult();
        assertThat(noMsgTestResult.getExecutionResult(), is(PASS));
        assertThat(noMsgTestResult.hasMessage(), is(false));
        assertThat(noMsgTestResult.getMessage(), is(nullValue()));
    }

    @Test
    public void should_print_toString() {
        assertThat(FixtureResult.parseFrom("pass:ok msg").toString(), is("pass:ok msg"));
        assertThat(FixtureResult.parseFrom(":msg").toString(), is(":msg"));
        assertThat(FixtureResult.parseFrom("ignore").toString(), is("ignore"));
    }
}