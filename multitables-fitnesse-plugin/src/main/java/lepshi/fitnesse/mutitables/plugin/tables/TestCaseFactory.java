package lepshi.fitnesse.mutitables.plugin.tables;

import com.google.common.annotations.VisibleForTesting;
import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;
import lepshi.fitnesse.mutitables.plugin.tables.AllExpectationsEvaluatedNotifyingCustomizer.AllExpectationsEvaluatedCallback;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory.ADJUSTMENT_FOR_HIGHLIGHTING;
import static lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType.TEST_CASE;
import static lepshi.fitnesse.mutitables.plugin.tables.ResultsPicker.pickFromResults;

@Slf4j
@RequiredArgsConstructor
class TestCaseFactory implements ActionCombinationFactory {

    private final EachTestSteps   eachTestSteps;
    private final SlimTestContext testContext;
    private final AtomicInteger   nextId = new AtomicInteger(0);

    @Override
    public CombinationType getCombinationType() {
        return TEST_CASE;
    }

    @Override
    public ActionCombination createActionCombination(Table titleTable, List<Table> actionTables) {
        final String testCaseId = TEST_CASE.getId() + nextId.getAndIncrement();
        return new ActionCombination(getCombinationType(),
                                     () -> testCaseId,
                                     titleTable,
                                     actionTables,
                                     assertionsCustomizer(titleTable, actionTables.get(0)),
                                     testContext);
    }


    @VisibleForTesting
    AssertionsCustomizer assertionsCustomizer(Table titleTable, Table stepsReportingTable) {
        return new StepsAttachingCustomizer(eachTestSteps, stepsReportingTable)
                .followedBy(new AllExpectationsEvaluatedNotifyingCustomizer(testCaseActionsEvaluator(titleTable)));
    }

    @VisibleForTesting
    AllExpectationsEvaluatedCallback testCaseActionsEvaluator(Table titleTable) {
        return actionResults -> evaluateTestCaseActions(titleTable, actionResults);
    }

    private void evaluateTestCaseActions(Table titleTable, List<TestResult> actionResults) {
        final String combinationTitle = titleTable.getCellContents(0, 0);
        final TableDataHighlighterFactory titleHighlighterFactory = new TableDataHighlighterFactory(titleTable);

        final ExecutionResult worstResult = pickFromResults(actionResults).adjusted(ADJUSTMENT_FOR_HIGHLIGHTING)
                                                                          .worst();
        LOG.debug("{} finished: {}", getCombinationType().decorateTitle(combinationTitle), worstResult.name());
        titleHighlighterFactory.table(worstResult).highlight();
    }

}
