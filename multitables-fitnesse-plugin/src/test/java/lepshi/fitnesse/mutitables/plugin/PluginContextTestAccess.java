package lepshi.fitnesse.mutitables.plugin;

import fitnesse.testsystems.slim.tables.SlimTableFactory;

import static lepshi.fitnesse.mutitables.plugin.PluginContext.pluginContext;


public class PluginContextTestAccess {

    public void setSlimTableFactory(SlimTableFactory factory) {
        pluginContext().setSlimTableFactory(factory);
    }
}
