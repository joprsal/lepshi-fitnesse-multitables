package lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.Table;
import lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;

import java.util.List;

interface ActionCombinationFactory {

    CombinationType getCombinationType();

    ActionCombination createActionCombination(Table titleTable, List<Table> actionTables);
}
